﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.IO;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            Random rnd = new Random();
            List<PajamosIslaidos> pajamosIrIslaidos = new List<PajamosIslaidos>();
            double vidAtlyginimas;
            double suma=0;
            double isuma = 0;
            double paskola=0;
            bool isgyvenimas = true;
            for (int i = 0; i < 12; i++)
            {
                pajamosIrIslaidos.Add(new PajamosIslaidos()
                {
                    Menesis = i+1,
                    Pajamos = rnd.Next(300, 1000),
                    Islaidos = rnd.Next(100,500)
                });
            }

            
            for(int i=0;i<12;i++)
            {
                suma = suma + pajamosIrIslaidos[i].Pajamos;
                isuma = isuma + pajamosIrIslaidos[i].Islaidos;
                if(isuma>suma)
                {
                    paskola = isuma - suma;
                }
            }

            vidAtlyginimas = suma / 12;

            if(suma<isuma)
            {
                isgyvenimas = false;
            }

            string folder = AppDomain.CurrentDomain.BaseDirectory + "../../AsmenuDuomenys/";
            InitializeComponent();

            var asmuo = new Asmuo()
            {
                Vardas = "Jonas",
                Pavarde = "Jonaitis",
                GimimoData = new DateTime(1985, 12, 11),
                Atlyginimas = vidAtlyginimas,
                Ugis = 183,
                isgyvens = isgyvenimas,
                Miestas = "Vilnius",
                Gatve = "Laisves pr.",
                NamoNr = 11,
                pajamosIrIslaidos = pajamosIrIslaidos,
                paskolosSuma = paskola
            };
            var Adresas = new Adresas()
            {
                
            };
            

            var json = JsonConvert.SerializeObject(asmuo);

            
            if(!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);

            }

            var filename = $"{asmuo.Vardas}_{asmuo.Pavarde}_duomenys.json";
            json = json;
            File.WriteAllText(folder + filename, json);
        }
    }

    public class Asmuo
    {
        public string Vardas { get; set; }

        public string Pavarde { get; set; }

        public DateTime GimimoData { get; set; }

        public double? Atlyginimas { get; set; }

        public int Ugis { get; set; }

        public bool isgyvens { get; set; }

        public string Miestas { get; set; }

        public string Gatve { get; set; }

        public int NamoNr { get; set; }

        public List<PajamosIslaidos> pajamosIrIslaidos { get; set; }

        public double paskolosSuma { get; set; }
    }
    public class Adresas
    {
        public string Miestas { get; set; }
        public string Gatve { get; set; }
        public int NamoNr { get; set; }
    }
    public class PajamosIslaidos
    {
        public int Menesis { get; set; }
        public int Pajamos { get; set; }
        public int Islaidos { get; set; }
    }
}
